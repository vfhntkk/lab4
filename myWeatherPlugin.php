<?php
/*
Plugin Name: myWeatherPlugin
Description: My weather plugin
Author: Me
Version: 1.0
*/

defined("ABSPATH") or die();

add_shortcode( "myWeather", "getMyWeather" );

function getMyWeather( $atts ) {
    
    try {
        // Обработка атрибутов шорткода
        // По умолчанию выводится погода в Архангельске на текущий момент
        // Или используются переданные значения
        $data = shortcode_atts( [
    		"city" => "Архангельск",
    		"mode" => "realtime",
    	], $atts );
    	
    	// Проверка корректности параметра mode
        if($data["mode"] != "forecast" && $data["mode"] != "realtime") {
            throw new Exception("Несуществующий режим");
        }
    	
    	// Формирование URL запроса
    	$url = 'https://api.tomorrow.io/v4/weather/' . $data["mode"];
    	
        // Формирование параметров запроса
        $params = array(
        	"location" => $data["city"],
        	"units" => "metric",
        	"apikey" => "2xgK3f83Z22bzGdm5rHtigzkNUJD1ZAI"
        );
        
        if($data["mode"] == "forecast") $params["timesteps"] = "1d";
        
        // корректная обработка кириллицы
        $params = urlencode_deep( $params );
        
        // Добавление параметров в URL
        $url = add_query_arg( $params, $url );
        
        // Отправка запроса
        $response = wp_remote_get( $url );
        
        // Анализируем код ответа
        if (wp_remote_retrieve_response_code( $response ) == 400) {
            throw new Exception("Несуществующий город или погода для этого города недоступна");
        }
        
        if (wp_remote_retrieve_response_code( $response ) == 429) {
            throw new Exception("Слишком много запросов. Попробуйте позже");
        }
        
        $body = json_decode(wp_remote_retrieve_body( $response ));
      
        if($data["mode"] == "realtime") {
            $html = "<p>Погода в городе " . $data["city"] . "  в данный момент: <br>Температура " . $body->data->values->temperature . "<sup>о</sup>С<br>Вероятность осадков " . $body->data->values->precipitationProbability . "%<br>Скорость ветра " . $body->data->values->windSpeed . " м/с</p>";
        }
        else {
            $html = "<p>Погода в городе " . $data["city"] . " на завтра: <br>Максимальная температура " . $body->timelines->daily[1]->values->temperatureMax . "<sup>о</sup>С<br>Вероятность осадков " . $body->timelines->daily[1]->values->precipitationProbabilityMax . "%<br>Максимальная скорость ветра " . $body->timelines->daily[1]->values->windSpeedMax . " м/с</p>";
        }
        
        return $html;
    }
    catch(Throwable $ex) {
        return "<p>" . $ex -> getMessage() . "</p>";
    }
}
?>